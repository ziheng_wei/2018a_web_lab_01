Herbivorous
===================
* Horse
* Cow
* Sheep
* Raccoon
* Chicken
* Capybara

Carnivorous
====================
* Cat
* Dog
* Polar Bear
* Hawk
* Lion
* Fox
* Fennec Fox

Omnivorous
====================
* Frog
* Rat
